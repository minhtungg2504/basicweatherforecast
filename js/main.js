const getEle = (id) => {
    return document.getElementById(id);
}

const main = () => {
    const address = document.getElementById("txtAddress").value;

    //dùng superagent call api của google, lấy tọa độ của địa chỉ của người dùng nhập 
    superagent.get(
        `https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDBunJ4GXNEC3KJlpoGJO-iB--CjPv4o-s&address=${address}`)
        .end((err, res)=>{
            if(err){
                console.log(err);
                return;
            }
            const {lat, lng} = res.body.results[0].geometry.location;
            console.log(lat,lng);
               
            superagent.get(
                `https://cors-anywhere.herokuapp.com/https://api.darksky.net/forecast/7bbecca28cbc31d7c6739e70baa64e46/${lat},${lng}`
            ).end((err, res) => {
                if(err){
                    console.log(err);
                    return;
                }
                const {summary, temperature} = res.body.currently;

                getEle("weatherText").innerHTML = summary;
                getEle("temperature").innerHTML = ((temperature-32)*5/9);
            })
        });
};

